const httpFunction= require('./index');
const context= require('../testing/Context');
const { expect } = require('@jest/globals');

test('Http trigger example' , async() => {
    const request = {
        query : { name: 'schuc'}
    };

    var iteration = 1000000;
    console.time('FUNCTION #1');
    for(var i = 0; i < iteration; i++) {
        httpFunction(context, request); 
    }
    console.timeEnd('FUNCTION #1');
    expect(context.res.body).toContain('W');
    expect(context.res.body).toEqual('Welcome, schuc');
    //expect(context.log.mock.calls.length).toBe(200);
});
